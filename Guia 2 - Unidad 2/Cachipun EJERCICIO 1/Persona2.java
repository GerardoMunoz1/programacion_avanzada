import java.util.Random;
import java.util.Scanner;

public class Persona2 {

	Scanner sc = new Scanner(System.in);

	public void juego() {

		Random randon = new Random();

		int puntosA = 0;
		int winA = 0; // victoria para jugador 1 mano izq
		int winB = 0; // victoria para bot mano izq
		int winC = 0; // victoria para jugador 1 mano der
		int winD = 0; // victoria para bot mano der
		int puntosB = 0;
		int jugadas = 0;
		int ganador = 0;
		int empates = 0;

		System.out.println("Jugando con la mano derecha contra CPU.");

		do {

			int random_izq;
			int random_der;
			random_izq = randon.nextInt(2);
			random_izq = random_izq + 1;

			random_der = randon.nextInt(2);
			random_der = random_der + 1;
			
			System.out.println("\nNUMERO RANDOM MANO DERECHA BOT: " + random_der);
			System.out.println("\nNUMERO RANDOM MANO IZQUIERDA BOT: " + random_izq);
			
			System.out.print("\n[MANO DERECHA] Escoja: piedra, papel o tijera(1,2,3): ");
			int mano_derecha_user = sc.nextInt();

			System.out.print("\n[MANO IZQUIERDA] Escoja: piedra, papel o tijera(1,2,3): ");
			int mano_izquierda_user = sc.nextInt();

			if ((mano_izquierda_user <= 3) && (mano_izquierda_user != 0) && (mano_derecha_user <= 3)
					&& (mano_derecha_user != 0)) { // verificación de la opcion ingresada en el intervalo pedido

				if ((mano_izquierda_user == 1) && (random_izq == 3)) { // piedra vs tijera (gana jugador 1)

					System.out.print("Jugador 1: PIEDRA");
					System.out.print("	BOT: TIJERA\n");

					System.out.println("\nGANA LA MANO IZQUIERDA DEL JUGADOR 1");
					//puntosA = puntosA + 1;
					
					winA = winA + 1;
					winB = winB + 0;

				} else if ((mano_izquierda_user == 2) && (random_izq == 1)) { // papel vs piedra (gana jugador 1)
					System.out.print("Jugador 1: PAPEL");
					System.out.print("	BOT: PIEDRA\n");

					System.out.println("\nGANA LA MANO IZQUIERDA DEL JUGADOR 1");
					//puntosA = puntosA + 1;
					winA = winA + 1;
					winB = winB + 0;

				} else if ((mano_izquierda_user == 3) && (random_izq == 2)) { // tijera vs papel (gana jugador 1)
					System.out.print("Jugador 1: TIJERA");
					System.out.print("	BOT: PAPEL\n");

					System.out.println("\nGANA LA MANO IZQUIERDA DEL JUGADOR 1");
					//puntosA = puntosA + 1;
					
					winA = winA + 1;
					winB = winB + 0;

				}

				else if ((mano_izquierda_user == 3) && (random_izq == 1)) { // tijera vs piedra (gana jugador 2) bot
					System.out.print("Jugador 1: TIJERA");
					System.out.print("	BOT: PIEDRA\n");

					System.out.println("\nGANA LA MANO IZQUIERDA DEL JUGADOR 2");
					//puntosB = puntosB + 1;
					
					winA = winA + 0;
					winB = winB + 1;
				}

				else if ((mano_izquierda_user == 2) && (random_izq == 3)) { // papel vs tijera (gana jugador 2) bot
					System.out.print("Jugador 1: PAPEL");
					System.out.print("	BOT: TIJERA\n");

					System.out.println("\nGANA LA MANO IZQUIERDA DEL JUGADOR 2");
					//puntosB = puntosB + 1;
					
					winA = winA + 0;
					winB = winB + 1;
				}

				else if ((mano_izquierda_user == 1) && (random_izq == 2)) { // piedra vs papel (gana jugador 2) bot
					System.out.print("Jugador 1: PIEDRA");
					System.out.print("	BOT: PAPEL\n");

					System.out.println("\nGANA LA MANO IZQUIERDA JUGADOR 2");
					//puntosB = puntosB + 1;
					
					winA = winA + 0;
					winB = winB + 1;
				}
				// MANO DERECHA
				if ((mano_derecha_user == 1) && (random_der == 3)) { // piedra vs tijera (gana jugador 1)

					System.out.print("Jugador 1: PIEDRA");
					System.out.print("	BOT: TIJERA\n");

					System.out.println("\nGANA LA MANO DERECHA DEL JUGADOR 1");
					//puntosA = puntosA + 1;
					
					winC = winC + 1;
					winD = winD + 0;

				} else if ((mano_derecha_user == 2) && (random_der == 1)) { // papel vs piedra (gana jugador 1)
					System.out.print("Jugador 1: PAPEL");
					System.out.print("	BOT: PIEDRA\n");

					System.out.println("\nGANA LA MANO DERECHA DEL JUGADOR 1");
					//puntosA = puntosA + 1;
					winC = winC + 1;
					winD = winD + 0;

				} else if ((mano_derecha_user == 3) && (random_der == 2)) { // tijera vs papel (gana jugador 1)
					System.out.print("Jugador 1: TIJERA");
					System.out.print("	BOT: PAPEL\n");

					System.out.println("\nGANA LA MANO DERECHA DEL JUGADOR 1");
					//puntosA = puntosA + 1;
					winC = winC + 1;
					winD = winD + 0;

				}

				else if ((mano_derecha_user == 3) && (random_der == 1)) { // tijera vs piedra (gana jugador 2) bot
					System.out.print("Jugador 1: TIJERA");
					System.out.print("	BOT: PIEDRA\n");

					System.out.println("\nGANA LA MANO DERECHA DEL JUGADOR 2");
					//puntosB = puntosB + 1;
					winC = winC + 0;
					winD = winD + 1;
				}

				else if ((mano_derecha_user == 2) && (random_der == 3)) { // papel vs tijera (gana jugador 2) bot
					System.out.print("Jugador 1: PAPEL");
					System.out.print("	BOT: TIJERA\n");

					System.out.println("\nGANA LA MANO DERECHA DEL JUGADOR 2");
					//puntosB = puntosB + 1;
					winC = winC + 0;
					winD = winD + 1;
				}

				else if ((mano_derecha_user == 1) && (random_der == 2)) { // piedra vs papel (gana jugador 2) bot
					System.out.print("Jugador 1: PIEDRA");
					System.out.print("	BOT: PAPEL\n");

					System.out.println("\nGANA LA MANO DERECHA JUGADOR 2");
					//puntosB = puntosB + 1;
					winC = winC + 0;
					winD = winD + 1;
				}				
				
				// COMPARACION VICTORIA FINAL
				if ( ((winA == 1) && (winC == 1)) || ((winB == 0) && (winD == 0)) ) { // ganar dos manos
					
					System.out.println("RONDA GANADA PARA EL JUGADOR 1.");
					puntosA = puntosA + 1;
					
				}
				
				else if ( ((winA == 1) && (winC == 0)) || ((winB == 1) && (winD == 0)) ) { // empate (pierde una mano, gana la otra)
					System.out.print("\nSE HA PRODUCIDO UN EMPATE!");
					puntosA = puntosA + 0;
					puntosB = puntosB + 0;
					empates = empates + 1;
					winA = 0;
					winC = 0;
				}
				
				else if ( ((winA == 0) && (winC == 0)) || ((winB == 1) && (winD == 1)) ) { // victoria para el bot con dos manos
					System.out.println("RONDA GANADA PARA EL JUGADOR 2.");
					puntosB = puntosB + 1;
				}

				System.out.println("\n-------------------------");
				System.out.println("    PUNTAJE ACTUAL ");
				System.out.println("-------------------------");
				System.out.println("Jugador 1: " + puntosA);
				System.out.println("BOT: " + puntosB);

				int diferencia_ptos = puntosA - puntosB;

				if (diferencia_ptos < 0) {
					diferencia_ptos = diferencia_ptos * (-1);
					System.out.println("Diferencia de puntaje: " + diferencia_ptos);
				}

				else {
					System.out.println("Diferencia de puntaje: " + diferencia_ptos);
				}

				if (diferencia_ptos == 3) {
					ganador = 1;

					if (puntosA > puntosB) {
						System.out.println("GANO EL JUGADOR 1 CON: " + puntosA + " PUNTOS.");
						System.out.println("TOTAL DE EMPATES: " + empates);
						System.out.println("\nJUGADOR 1: (/°u°)/");
						System.out.println("BOT: (°n°)");
						System.out.println("      |  |");
					} else if (puntosA < puntosB) {
						System.out.println("GANO EL JUGADOR 2 CON: " + puntosB + " PUNTOS.");
						System.out.println("TOTAL DE EMPATES: " + empates);
						System.out.println("\nBOT: (/°u°)/");
						System.out.println("JUGADOR 1: (°n°)");
						System.out.println("           |  |");

					}
				}

				jugadas = jugadas + 1;

			}

			else {
				ganador = 0;
				System.out.println("\nOpcion ingresada no válida. Inténtelo nuevamente.");

			}

		} while (ganador == 0);

	}
}
