import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Persona1 persona1 = new Persona1();
		Persona2 persona2 = new Persona2();
		
		Scanner sc = new Scanner(System.in);
		
		int jugadas_por_modo = 0;
		String repetir;
		String partida; // partida = 1 (ON), partida = 0 (OFF)
		
		do {
			
			System.out.println("\nMENU");
			System.out.println("1. Jugar con una mano.");
			System.out.println("2. Jugar con dos manos.");
			System.out.print("\nOpcion: ");
			int opcion = sc.nextInt();
			
			System.out.println("\nOPCION INGRESADA: " + opcion);
			
			switch (opcion){
				case 1:
					
					do {
						System.out.println("Elegiste jugar con una sola mano.");
						
						persona1.leerMano();
		
						System.out.print("\n¿DESEA CONTINUAR? (s/n): ");
						repetir = sc.next();
						jugadas_por_modo = jugadas_por_modo + 1;
						
					}while ( (repetir.equals ("S")) || (repetir.equals ("s")) );
					break;
					
				case 2:
					
					do {
					
						System.out.println("\nElegiste jugar con dos manos.");
							
						persona2.juego();
						
						System.out.print("\n¿DESEA CONTINUAR? (s/n): ");
						repetir = sc.next();
						
						jugadas_por_modo = jugadas_por_modo + 1;
						
					}while ( (repetir.equals ("S")) || (repetir.equals ("s")) );
					break;
				
				default:
					
					System.out.print("Debes ingresar un número (1 o 2).");
					break;
				
			}
			
			System.out.print("\n¿DESEA EMPEZAR DENUEVO? (s/n): ");
			partida = sc.next();
			
		}while ( (partida.equals ("S")) || (partida.equals ("s")) );
		
		System.out.println("\nSALISTE.");
		
		System.out.println("JUGADAS TOTALES POR MODO: " + jugadas_por_modo);

	}

}
