import java.util.Random;
import java.util.Scanner;

public class Persona1 {
	
	Scanner sc = new Scanner(System.in);
	
	public void leerMano() {
		
		Random randon = new Random();
		
		int puntosA = 0;
		int puntosB = 0;
		int jugadas = 0;
		int ganador = 0;
					
		System.out.println("Jugando con la mano derecha contra CPU.");
			
		do {
			
			int random;
			random = randon.nextInt(2);
			random = random + 1;
			System.out.println("\nNUMERO RANDOM: " + random);
			
			System.out.print("\nEscoja: piedra, papel o tijera(1,2,3): ");
			int jugada = sc.nextInt();
			
			if ( (jugada <= 3) && (jugada != 0) ) { // verificación de la opcion ingresada en el intervalo pedido
				if ( (jugada == 1) && (random == 3) ) { // piedra vs tijera (gana jugador 1)
					
					System.out.print("Jugador 1: PIEDRA");
					System.out.print("	BOT: TIJERA\n");
					
					System.out.println("\nGANA JUGADOR 1");
					puntosA = puntosA + 1;
					
				}
				else if ( (jugada == 2) && (random == 1) ) { //papel vs piedra (gana jugador 1)
					System.out.print("Jugador 1: PAPEL");
					System.out.print("	BOT: PIEDRA\n");
					
					System.out.println("\nGANA JUGADOR 1");
					puntosA = puntosA + 1;
					
				}
				else if ( (jugada == 3) && (random == 2) ) { // tijera vs papel (gana jugador 1)
					System.out.print("Jugador 1: TIJERA");
					System.out.print("	BOT: PAPEL\n");
					
					System.out.println("\nGANA JUGADOR 1");
					puntosA = puntosA + 1;
					
				}
				
				else if ( (jugada == 3) && (random == 1) ) { // tijera vs piedra (gana jugador 2) bot
					System.out.print("Jugador 1: TIJERA");
					System.out.print("	BOT: PIEDRA\n");
					
					System.out.println("\nGANA JUGADOR 2");
					puntosB = puntosB + 1;
				}
				
				else if ( (jugada == 2) && (random == 3) ) { // papel vs tijera (gana jugador 2) bot
					System.out.print("Jugador 1: PAPEL");
					System.out.print("	BOT: TIJERA\n");
					
					System.out.println("\nGANA JUGADOR 2");
					puntosB = puntosB + 1;
				}
				
				else if ( (jugada == 1) && (random == 2) ) { // piedra vs papel (gana jugador 2) bot
					System.out.print("Jugador 1: PIEDRA");
					System.out.print("	BOT: PAPEL\n");
					
					System.out.println("\nGANA JUGADOR 2");
					puntosB = puntosB + 1;
				}
				
				else {
					System.out.print("\nSE HA PRODUCIDO UN EMPATE!");
					puntosA = puntosA + 0;
					puntosB = puntosB + 0;
				}
				
				System.out.println("\n-------------------------");
				System.out.println("    PUNTAJE ACTUAL ");
				System.out.println("-------------------------");
				System.out.println("Jugador 1: " + puntosA);
				System.out.println("BOT: " + puntosB);
				
				int diferencia_ptos = puntosA - puntosB;
				
				if (diferencia_ptos < 0) {
					diferencia_ptos = diferencia_ptos * (-1);
					System.out.println("Diferencia de puntaje: " + diferencia_ptos);
				}
				
				else {
					System.out.println("Diferencia de puntaje: " + diferencia_ptos);
				}
				
				if (diferencia_ptos == 3) {
					ganador = 1;
					
					if (puntosA > puntosB) {
						System.out.println("GANO EL JUGADOR 1 CON: " + puntosA + " PUNTOS.");
						System.out.println("JUGADOR 1: (/°u°)/");
						System.out.println("BOT: (°n°)");
						System.out.println("      |  |");
					}
					else if (puntosA < puntosB) {
						System.out.println("GANO EL JUGADOR 2 CON: " + puntosB + " PUNTOS.");
						System.out.println("BOT: (/°u°)/");
						System.out.println("JUGADOR 1: (°n°)");
						System.out.println("           |  |");
						
					}
				}
				
				jugadas = jugadas + 1;
				
			}
			
			else {
				ganador = 0;
				System.out.println("\nOpcion ingresada no válida. Inténtelo nuevamente.");
				
			}
			
			
			
		}while (ganador == 0);
			
			
	}
		
}
	

