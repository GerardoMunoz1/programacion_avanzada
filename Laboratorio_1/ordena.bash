#!/bin/bash -x

directorio_ingresado=$1

if [[ ! -d "$directorio_ingresado" ]]; then
	echo
	echo "ERROR."
	echo "INGRESE UN DIRECTORIO E INTENTELO NUEVAMENTE."
	echo
	exit 1

else
	echo "VAMOS."
	sleep 0.5
	chmod +x ordena.bash
	echo
	echo "USTED SE ENCUENTRA EN: $PWD"
	echo "-----------------------------------------------------------"
	echo "[LISTA COMPLETA]"
	echo
	ls -l ./$directorio_ingresado
	echo "-----------------------------------------------------------"
	echo "[LISTA SOLO TAMANO]"
	ls -l ./$directorio_ingresado |awk '{print $5}'
	echo "-----------------------------------------------------------"
	echo "Directorio ingresado: "$1
	echo "-----------------------------------------------------------"
	echo
	sleep 0.5
	echo "TE ENCUENTRAS EN: $PWD"
	sleep 1
	echo
	echo "MOVIENDO A DIRECTORIO INGRESADO..."
	sleep 1
	cd $directorio_ingresado
	echo
	echo "AHORA ESTAS EN: $PWD"
	echo 
fi

echo "MOSTRANDO LISTADO..."
sleep 1
ls -l --sort=size -r |awk '{print $9}' |nl
echo