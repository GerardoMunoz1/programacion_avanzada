#!/bin/bash -x

parametro_ingresado=$1

if [[ $2 ]]; then
	echo "Error. Solo se admite un parametro."
	exit 1
fi

if [[ ($parametro_ingresado == '-s') || ($parametro_ingresado == '--short') ]]; then
	
	echo
	echo `date +%d/%m/%Y`
	echo
elif [[ ($parametro_ingresado == '-l') || ($parametro_ingresado == '--long') ]]; then
	
	echo
	echo "Hoy es el día `date +%d` del mes `date +%m` del año `date +%Y` ."
	echo

elif [[ $# == 0 ]]; then
	cal

elif [[ ($parametro_ingresado -ne '-s') || ($parametro_ingresado -ne '--short') || ($parametro_ingresado -ne '-l') || ($parametro_ingresado -ne '--long') ]]; then
	echo "Parametro ingresado no válido."
	exit 1

fi
