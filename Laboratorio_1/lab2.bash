#!/bin/bash -x

parametro_directorio=$1

cd
echo "-----------------------------------------------------------"
echo "RUTA ACTUAL: $PWD"
echo "-----------------------------------------------------------"
echo "DIRECTORIO INGRESADO: $parametro_directorio"
echo

if [[ $# == 0 ]]; then

	if [[ ! -d "DEFAULT" ]]; then
		echo "Se ha creado una carpeta por defecto en: $PWD ."
		echo "CARPETA CREADA: DEFAULT"
		mkdir DEFAULT
		echo "todo ok"
		echo "-----------------------------------------------------------"
		cd $HOME/$DEFAULT
		echo "RUTA ACTUAL: $PWD"

		touch 1t4v_docking.mol2
		touch 1t4v_referencia.mol2
		touch 2r2m_docking.mol2
		touch 2r2m_referencia.mol2
		touch 3ldx_docking.mol2
		touch 3ldx_docking_new.mol2
		touch 3ldx_referencia.mol2
		touch 3ldx_referencia_new.mol2

		mkdir Carpeta_1
		mkdir Carpeta_2
		mkdir Carpeta_3

		ls -l

	else
		echo "Error, el directorio ya existe."
		echo "-----------------------------------------------------------"
		cd $DEFAULT
		echo "RUTA ACTUAL: $PWD"

		ls -l
		
	fi

else
	if [[ ! -d "$parametro_directorio" ]]; then
		echo "Se creara la carpeta: $parametro_directorio ."
		mkdir $parametro_directorio
		echo "todo ok"
		echo "-----------------------------------------------------------"
		echo
		cd $parametro_directorio
		echo "RUTA ACTUAL: $PWD"

		touch 1t4v_docking.mol2
		touch 1t4v_referencia.mol2
		touch 2r2m_docking.mol2
		touch 2r2m_referencia.mol2
		touch 3ldx_docking.mol2
		touch 3ldx_docking new.mol2
		touch 3ldx_referencia.mol2
		touch 3ldx_referencia_new.mol2

		mkdir Carpeta_1
		mkdir Carpeta_2
		mkdir Carpeta_3

		ls -l

	else
		echo "Error, el directorio ya existe."
		echo "-----------------------------------------------------------"
		cd $parametro_directorio
		echo "RUTA ACTUAL: $PWD"

		ls -l
	fi

fi
