package com.example.api_converter;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MainActivity extends AppCompatActivity {

    ProgressDialog pd;

    public void onClick_btnCalendar(View view) {

        final TextView texto = (TextView) findViewById(R.id.txt_view);
        final CalendarView calendar = (CalendarView) findViewById(R.id.calendarApp);
        calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView calendar, int i, int i1, int i2) {

                String fecha = i2 + "/" + (i1 + 1) + "/" + i;

                texto.setText(fecha);
            }
        });
    }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);
        }
    }