BEGIN {
    print "digraph G {"
    print "rankdir=LR;"
    print "node [shape=circle]"
    nuevo = ""
}
{

    z= $3$2
    comit="\""

	flag = $4

	for (k=0 ; k<NR ; k++){
		print NR
		
		if (nuevo != flag){

			if (NR == 1){
				nuevo = $4
				print "node [shape=box]"
				print nuevo " -> " flag "[ label = Cadena "$5" ]"
			}

			else{
				print "node [shape=box]"
				print nuevo " -> " flag "[ label = Cadena "$5" ]"
				nuevo = $4
			}
		}
	}
}

END {
	print "}"
}