#!/bin/bash -x
clear
echo
echo "RUTA: $PWD"
echo "----------------------------------------------------------------------------------------------------------------------"

interruptor=0

function volver_a_intentar(){

	echo -n "¿DESEA CONTINUAR? (s/n): "
	read reintento

	if [[ ($reintento == 's') || ($reintento == 'S') ]]; then
		clear
		echo
		echo "RUTA: $PWD"
		echo "----------------------------------------------------------------------------------------------------------------------"
		echo "VAMOS"
		echo
		let interruptor=interruptor+0

	elif [[ ($reintento == 'n') || ($reintento == 'N') ]]; then
		let interruptor=interruptor+1
		echo
		echo "HASTA PRONTO ;)."

	else
		echo
		echo "LETRA INGRESADA NO VÁLIDA."
		echo "HASTA PRONTO ;)."
		let interruptor=interruptor+1
	fi
}

function comprobar_archivo(){

	if [[ ! -f "$codigo.pdb" ]]; then

		sleep 0.3
		echo "DESCARGANDO..."
		echo
		wget https://files.rcsb.org/download/"$codigo".pdb
		echo
		echo "DESCARGA FINALIZADA CON ÉXITO..."
		sleep 0.3
		echo
		echo
		volver_a_intentar
	else
		echo "EL ARCHIVO \"$codigo.pdb\" YA EXISTE."
		volver_a_intentar
	fi

}

function comprobar_proteina(){

	if [[ "$tipo" == "\"Protein\"" ]]; then

		comprobar_archivo $codigo
	
	else
		echo "EL CÓDIGO INGRESADO NO CORRESPONDE A UNA PROTEÍNA."
		sleep 0.3
		echo "INTENTELO NUEVAMENTE."
		echo
		volver_a_intentar

	fi

}

function buscar_coincidencia {

	if [[ "$coincidencia" == "$mayus" ]]; then
		echo "----------------------------------------------------------------------------------------------------------------------"
		echo "SE ENCONTRÓ COINCIDENCIA CON EL CÓDIGO: $coincidencia"
		sleep 0.3
		echo

		categoria=$(cat bd-pdb.txt |awk -F "," '{print $mayus}' | grep $mayus)
		tipo=$(echo $categoria |awk -F "," '{print $(NF)}').
		
		echo "----------------------------------------------------------------------------------------------------------------------"
		echo
		echo "CÓDIGO: [$coincidencia]."
		sleep 0.5
		echo
		echo "DESCRIPCIÓN: $categoria."
		sleep 0.5
		echo
		echo "CATEGORíA: $tipo"
		echo
		echo "----------------------------------------------------------------------------------------------------------------------"
		sleep 0.5

		tipo=${tipo:0:-1}
		echo

		comprobar_proteina $tipo

	else
		echo "----------------------------------------------------------------------------------------------------------------------"
		echo "NO SE ENCONTRÓ COINCIDENCIA CON EL CÓDIGO INGRESADO."
		echo
		echo "----------------------------------------------------------------------------------------------------------------------"
		volver_a_intentar
	fi

}

while [ $interruptor -ne 1 ]; do
	echo
	read -p "INTRODUZCA UN CÓDIGO: " codigo
	echo
	mayus=$(echo $codigo | tr 'a-z' 'A-Z')

	if [[ ${#codigo} -ne 4 ]]; then
		echo "----------------------------------------------------------------------------------------------------------------------"
		echo "ERROR. CÓDIGO INGRESADO NO VÁLIDO."
		echo "POR FAVOR, INGRESAR CÓDIGO DE 4 CARACTERES."
		echo
		echo "----------------------------------------------------------------------------------------------------------------------"		
		echo -n "¿REINTENTAR? (s/n): "
		read reintento
		#reintento=`read -p "¿REINTENTAR? (s/n): "`
		#echo "LETRA REINTENTO: $reintento"
		echo
		echo "----------------------------------------------------------------------------------------------------------------------"
		
		if [[ ($reintento == 's') || ($reintento == 'S') ]]; then
			clear
			echo
			echo "RUTA: $PWD"
			echo "----------------------------------------------------------------------------------------------------------------------"
			echo "VAMOS"
			echo
			let interruptor=interruptor+0

		elif [[ ($reintento == 'n') || ($reintento == 'N') ]]; then
			let interruptor=interruptor+1
			echo
			echo "HASTA PRONTO ;)."

		else
			echo
			echo "LETRA INGRESADA NO VÁLIDA."
			echo "HASTA PRONTO ;)."
			let interruptor=interruptor+1
		fi

	else
		echo "ABRIENDO BASE DE DATOS..."
		sleep 0.3

		cat -b bd-pdb.txt |awk -F "," '{print $1 " | " $4}'
		echo
		echo "----------------------------------------------------------------------------------------------------------------------"
		echo "CÓDIGO INGRESADO: $mayus"
		echo "----------------------------------------------------------------------------------------------------------------------"
		echo
		sleep 0.3
		echo "BUSCANDO CÓDIGO EN LA BASE DE DATOS..."
		sleep 0.3
		echo "DONE"

		#echo $mayus // linea para comprobar si está funcionando mayus.

		coincidencia=$(cat bd-pdb.txt |awk -F "," '{print $1}' | grep $mayus)
		#echo $coincidencia // linea para comprobar si está funcionando mayus.

		mayus="\"$mayus\""

		buscar_coincidencia $coincidencia $mayus

	fi
done
