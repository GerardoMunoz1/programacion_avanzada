#!/bin/bash -x
clear
echo
interruptor=0

function volver_a_intentar(){

	echo -n "¿DESEA CONTINUAR? (s/n): "
	read reintento

	if [[ ($reintento == 's') || ($reintento == 'S') ]]; then
		clear
		echo
		echo "RUTA: $PWD"
		echo "----------------------------------------------------------------------------------------------------------------------"
		echo "VAMOS"
		echo
		let interruptor=interruptor+0

	elif [[ ($reintento == 'n') || ($reintento == 'N') ]]; then
		let interruptor=interruptor+1
		echo
		echo "HASTA PRONTO ;)."

	else
		echo
		echo "LETRA INGRESADA NO VÁLIDA."
		echo "HASTA PRONTO ;)."
		let interruptor=interruptor+1
	fi
}

function recorrer_txt(){

	anterior=""
	a=$(cat arch_$codigo.txt |awk '{print $4}') # columna aminoacidos
	b=$(cat arch_$codigo.txt |awk '{print $3}') # columna elementos
	c=$(cat arch_$codigo.txt |awk '{print $5}') # columna cadena

	for x in $a; do

		if [[ $anterior != $x ]]; then
			echo $x
			echo "$anterior ----> $x"
			
			rellenar_dot $x $b $c
			anterior=$x
		fi
	done
}

function comprobar_archivo(){

	if [[ ! -f "$codigo.pdb" ]]; then
		sleep 0.3
		echo "DESCARGANDO..."
		echo
		wget https://files.rcsb.org/download/"$codigo".pdb
		echo
		echo "DESCARGA FINALIZADA CON ÉXITO..."
		sleep 0.3
		echo
		echo "GENERANDO IMAGEN..."
		echo
		cat $codigo.pdb |grep ^ATOM |awk '{print (substr($1,1,4) " " substr($2,1,3) " " substr($3,1,3) " " substr($0,18,20))}' > arch_$codigo.txt
		cp arch_$codigo.txt $PWD/../
		cd $PWD/../
		#recorrer_txt $(arch_$codigo.txt)
		cat arch_$codigo.txt |awk -f graphic.awk >> file.dot
		dot -Tsvg -o $codigo.svg "file.dot"

		menu

	else
		echo "EL ARCHIVO \"$codigo.pdb\" YA EXISTE."
		menu
	fi
}

function comprobar_proteina(){

	if [[ ! -d "proteinas" ]];then
		echo
		echo "CREANDO CARPETA: \"proteinas\""
		mkdir $PWD/proteinas
		cd proteinas

		if [[ "$tipo" == "\"Protein\"" ]]; then

			comprobar_archivo $codigo
		
		else
			echo "EL CÓDIGO INGRESADO NO CORRESPONDE A UNA PROTEÍNA."
			sleep 0.3
			echo "INTENTELO NUEVAMENTE."
			echo
			volver_a_intentar
		fi

	else
		echo
		echo "LA CARPETA \"proteinas\" YA EXISTE."
		cd proteinas

		if [[ "$tipo" == "\"Protein\"" ]]; then

			comprobar_archivo $codigo
		
		else
			echo "EL CÓDIGO INGRESADO NO CORRESPONDE A UNA PROTEÍNA."
			sleep 0.3
			echo "INTENTELO NUEVAMENTE."
			echo
			volver_a_intentar
		fi
	fi
}

function buscar_coincidencia {

	if [[ "$coincidencia" == "$mayus" ]]; then
		echo "----------------------------------------------------------------------------------------------------------------------"
		echo "SE ENCONTRÓ COINCIDENCIA CON EL CÓDIGO: $coincidencia"
		sleep 0.3
		echo
		categoria=$(cat bd-pdb.txt |awk -F "," '{print $mayus}' | grep $mayus)
		tipo=$(echo $categoria |awk -F "," '{print $(NF)}').
		echo "----------------------------------------------------------------------------------------------------------------------"
		echo
		echo "CÓDIGO: [$coincidencia]."
		sleep 0.5
		echo
		echo "DESCRIPCIÓN: $categoria."
		sleep 0.5
		echo
		echo "CATEGORíA: $tipo"
		echo
		echo "----------------------------------------------------------------------------------------------------------------------"
		sleep 0.5
		tipo=${tipo:0:-1}
		echo
		comprobar_proteina $tipo

	else
		echo "----------------------------------------------------------------------------------------------------------------------"
		echo "NO SE ENCONTRÓ COINCIDENCIA CON EL CÓDIGO INGRESADO."
		echo
		echo "----------------------------------------------------------------------------------------------------------------------"
		volver_a_intentar
	fi
}

function main(){

	while [ $interruptor -ne 1 ]; do

		echo "----------------------------------------------------------------------------------------------------------------------"
		cd $RUTA_PRINCIPAL
		read -p "INTRODUZCA UN CÓDIGO: " codigo
		echo
		mayus=$(echo $codigo | tr 'a-z' 'A-Z')

		if [[ ${#codigo} -ne 4 ]]; then
			echo "----------------------------------------------------------------------------------------------------------------------"
			echo "ERROR. CÓDIGO INGRESADO NO VÁLIDO."
			echo "POR FAVOR, INGRESAR CÓDIGO DE 4 CARACTERES."
			echo
			echo "----------------------------------------------------------------------------------------------------------------------"		
			echo -n "¿REINTENTAR? (s/n): "
			read reintento
			echo
			echo "----------------------------------------------------------------------------------------------------------------------"
			
			if [[ ($reintento == 's') || ($reintento == 'S') ]]; then
				clear
				echo
				cd $RUTA_PRINCIPAL
				echo "----------------------------------------------------------------------------------------------------------------------"
				echo "VAMOS"
				echo
				let interruptor=interruptor+0

			elif [[ ($reintento == 'n') || ($reintento == 'N') ]]; then
				let interruptor=interruptor+1
				echo
				echo "HASTA PRONTO ;)."

			else
				echo
				echo "LETRA INGRESADA NO VÁLIDA."
				echo "HASTA PRONTO ;)."
				let interruptor=interruptor+1
			fi

		else
			echo
			echo "----------------------------------------------------------------------------------------------------------------------"
			echo "CÓDIGO INGRESADO: $mayus"
			echo "----------------------------------------------------------------------------------------------------------------------"
			echo
			cd $RUTA_PRINCIPAL
			sleep 0.3
			echo "BUSCANDO CÓDIGO EN LA BASE DE DATOS..."
			sleep 0.3
			echo "DONE"

			#echo $mayus // linea para comprobar si está funcionando mayus.

			coincidencia=$(cat bd-pdb.txt |awk -F "," '{print $1}' | grep $mayus)
			#echo $coincidencia // linea para comprobar si está funcionando mayus.

			mayus="\"$mayus\""

			buscar_coincidencia $coincidencia $mayus
		fi
	done
}

function eleccion(){

	if [[ $option == '1' ]]; then
		echo "ABRIENDO BASE DE DATOS..."
		sleep 0.3
		cat -b bd-pdb.txt |awk -F "," '{print $1 " | " $4}'
		menu

	elif [[ $option == '2' ]]; then
		main

	elif [[ $option == '3' ]]; then
		cd proteinas
		
		if [[ ! -d "proteinas" ]]; then
			clear
			echo "----------------------------------------------------------------------------------------------------------------------"
			echo "RUTA: $PWD"
			echo
			ls -l |awk '{print $9}' |grep pdb$
			echo
			read -p "ESCRIBA 1 PARA VOLVER AL MENU PRINCIPAL: " volvermenu

			if [[ $volvermenu == '1' ]]; then
				clear
				cd $RUTA_PRINCIPAL
				menu
			fi

		else
			clear
			echo "RUTA: $PWD"
			echo
			echo "NO EXISTE EL DIRECTORIO."
			echo "NO SE PRESENTAN PROTEINAS DESCARGADAS."
			menu
		fi

	elif [[ $option == '4' ]]; then
		echo
		echo "Hasta pronto!"
		exit 1

	else
		clear
		echo "Error"
		menu
	fi
}

function menu(){
	echo
	cd $RUTA_PRINCIPAL
	echo "RUTA: $PWD"
	echo "+--------------------------------------------------------------------------------------------------------------------+"
	echo "|                                           GRAFICADOR DE PROTEÍNAS                                                  |"
	echo "|                                                  Beta Version                                                      |"
	echo "+--------------------------------------------------------------------------------------------------------------------+"
	echo
	echo "1. VER BASE DE DATOS."
	echo "2. DESCARGAR UNA PROTEÍNA."
	echo "3. VER PROTEÍNAS DISPONIBLES."
	echo "4. SALIR."
	echo
	read -p "INGRESE UNA OPCIÓN: " option
	eleccion $option
}

RUTA_PRINCIPAL="$PWD"
menu
