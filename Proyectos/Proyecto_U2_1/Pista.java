import java.util.Random;

public class Pista {

	private int longitud = 5000; //5000 m, para 1 min aprox
	Random randon = new Random();
	
	private int zona;
	
	private void definirZona () {
		
		zona = randon.nextInt(100);
		
		System.out.println("ZONA ESCOGIDA: " + zona);
		
		if (zona <= 50) {
			System.out.println("ZONA: [TERRESTRE]");
			
		}
		else if (zona > 50) {
			System.out.println("ZONA: [AEREA]");
		}
		
	}

	public int getZona() {
		definirZona();
		return zona;
	}

	public void setZona(int zona) {
		this.zona = zona;
	}
	
	
}
