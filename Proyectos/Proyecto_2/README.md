# Graficador de estructuras de proteínas

## Explicacion del proyecto

Para abarcar el problema planteado, se utilizó Bash Script, en donde se procesan, filtran y analizan diferentes tipos de archivos. Las diferentes condiciones que fueron establecidas, se demuestran en lo siguiente:

* Solo ejecutar archivos con extensión ".pdb".
* Utilizar solamente archivos descargados desde la base de datos entregada (https://icb.utalca.cl/~fduran/bd-pdb.txt).

El objetivo del proyecto, es evidenciar gráficamente todos los aminoácidos contenidos en un radio medido en angstrom, estimado por el usuario.

### Pre-requisitos

Para ejecutar el script, se necesita tener una conexión a Internet, un navegador, instalar el paquete "graphviz", en caso de no tenerlo:

```
sudo apt-get install graphviz
```

### Ejecución

Antes de ejecutar el script, se debe tener en una misma carpeta "calculo_centro.awk",
"graficador.awk" y "proyecto2.sh".

Se debe abrir una terminal y ejecutar:

```
bash proyecto2.sh
```

## Construido con


* [AWK] - Herramienta para filtrar por las columnas deseadas.
* [graphviz](https://www.graphviz.org/) - Graficador de archivos con extensión ".dot".
* [BASH SCRIPT](https://devhints.io/bash) - Base de todo el código, ejecuta todas las instrucciones. 


## Autores

* **Gerardo Muñoz**

## Contribuyentes

Se utilizó parte de la solución propuesta por el siguiente autor:

* **Benjamín Sanzana**
