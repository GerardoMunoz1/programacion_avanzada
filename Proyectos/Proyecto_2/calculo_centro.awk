BEGIN {
    suma_x = $7
    suma_y = $8
    suma_z = $9
    new = ""
    n = 0
    flag = 0
}

{
    dif_aa=($4$6)

    if (new != dif_aa && flag == 0){

        if (NR == 1){
            suma_x = 0
            suma_y = 0
            suma_z = 0
            flag = 1
            new = dif_aa
            n = 0
        }
        else{
            suma_x = (suma_x) / n
            suma_y = (suma_y) / n
            suma_z = (suma_z) / n

            print "DATOS FINALES: " new, suma_x, suma_y, suma_z
            suma_x = 0
            suma_y = 0
            suma_z = 0
            flag = 1
            new = dif_aa
            n = 0
        }
    }

    print new " " $7 " " $8 " " $9
    suma_x = (suma_x + $7)
    suma_y = (suma_y + $8)
    suma_z = (suma_z + $9)
    flag = 0
    new = dif_aa
    n = n + 1
    #print "CODIGO CAMBIO: " new

    #print suma_x
    #print suma_y
    #print suma_z
    #print "LLEVAMOS: " n
    #print "--------------------------------------------------------"

}

END {
    suma_x = (suma_x) / n
    suma_y = (suma_y) / n
    suma_z = (suma_z) / n
    print "DATOS FINALES: " new, suma_x, suma_y, suma_z
}
